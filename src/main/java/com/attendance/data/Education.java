package com.attendance.data;

public class Education {
private long id;
private String institution;
private String start;
private String end;
private String dateAdded;
private String cetification;
public long getId() {
	return id;
}
public void setId(long id) {
	this.id = id;
}
public String getInstitution() {
	return institution;
}
public void setInstitution(String institution) {
	this.institution = institution;
}
public String getStart() {
	return start;
}
public void setStart(String start) {
	this.start = start;
}
public String getEnd() {
	return end;
}
public void setEnd(String end) {
	this.end = end;
}
public String getDateAdded() {
	return dateAdded;
}
public void setDateAdded(String dateAdded) {
	this.dateAdded = dateAdded;
}
public String getCetification() {
	return cetification;
}
public void setCetification(String cetification) {
	this.cetification = cetification;
}

}
